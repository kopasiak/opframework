<?php
	$page_config = [
		'dev' => true,
		'url' => 'http://moje.opsoft.localhost'
	];

	$db_config = [
		'db_host' => "",
		'db_name' => '',
		'db_user' => '',
		'db_pass' => ''
	];

	define('PAGE_CONFIG',$page_config);
	define('DB_CONFIG',$db_config);
