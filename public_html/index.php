<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require '../core/Autoloader.php';
(new Autoloader())->load();

use Core\Utils\{Router, Request, Auth};

$router = Router::load('../routes.php');
if(DB_CONFIG["db_host"] && DB_CONFIG["db_name"]){
    Db::getConn();
}

Auth::checkAuthSession();

$router->direct(Request::uri(), Request::method());

