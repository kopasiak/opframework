<?php

use Core\Utils\Auth;

$query = "
CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    active TINYINT DEFAULT 1,
    created_at DATETIME NOT NULL
)  ENGINE=INNODB;

INSERT INTO users VALUES (1,'kamilopasiak@gmail.com','Kamil','".Auth::hash('test')."',1,NOW());
";