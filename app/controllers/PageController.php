<?php

namespace App\Controllers;

use App\Models\PageModel;

class PageController
{

	public function index()
	{
		$model = new PageModel();
		$data = $model->getAll();
		return view('index',['data' => $data]);
	}
	

}
