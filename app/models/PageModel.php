<?php 

namespace App\Models;

use Core\App\Model;
use PDO;

class PageModel extends Model{
    
    public function getAll(){
        if(DB_CONFIG["db_host"] && DB_CONFIG["db_name"]) {
            return $this->db->query("SELECT * from pages")->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return [
                ["name" => "Page 1"],
                ["name" => "Page 2"],
            ];
        }
    }

}