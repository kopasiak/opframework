<?php

namespace App\Models;

use Core\App\Model;
use PDO;

class UserModel extends Model
{

    public function getAll()
    {
        return $this->db->query("SELECT * from users")->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getUser($id)
    {
        return $this->db->query("SELECT * from users WHERE id = ".intval($id))->fetch(PDO::FETCH_ASSOC);
    }
}
