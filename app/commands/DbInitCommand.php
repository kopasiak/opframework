<?php

namespace App\Command;

use Core\App\Command;
use Db;
use PDOException;

class DbInitCommand extends Command
{

    public function run()
    {
        $db = Db::getConn();
        try {
            $db->beginTransaction();
            $sqlDirectory = scandir("sql");
            foreach($sqlDirectory as $item)
            {
                if(strpos($item,".php") > 0)
                {
                    require "sql/".$item;
                    $db->exec($query);
                    echo $query.PHP_EOL;
                }
            }
            $db->commit();
        } catch(PDOException $e) {
            echo "ERROR: ROLLBACK";
            $db->rollback();
        }

    }
}
