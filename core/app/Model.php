<?php

namespace Core\App;

use Db;

class Model
{
    protected $db;

    public function __construct()
    {
        $this->db = Db::getConn();
    }
}
