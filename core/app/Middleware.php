<?php 

namespace Core\App;

abstract class Middleware{

    protected $_request;

    public function __construct()
    {
        $this->_request = Request::get();
    }

    abstract public function check();

    public function callback(){
        return redirectCode('404');
    }
}