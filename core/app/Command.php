<?php 

namespace Core\App;

abstract class Command{

    protected $_args;

    public function __construct($args)
    {
        $this->_args = $args;
    }

    abstract public function run();

}