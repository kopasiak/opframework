<?php

namespace Core\App\Middleware;

use Core\Utils\Auth;
use Core\App\Middleware;

class GlobalMiddleware extends Middleware
{

    public function check()
    {
        session("old_data",array_merge($this->_request->post,session("old_data")));
        return true;
    }

    public function callback()
    {
        return redirect("404");
    }
}
