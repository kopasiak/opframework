<?php

namespace Core\App\Middleware;

use Core\Utils\Auth;
use Core\App\Middleware;

class AuthMiddleware extends Middleware
{

    public function check()
    {
        return Auth::checkAuth();
    }

    public function callback()
    {
        return redirect("login");
    }
}
