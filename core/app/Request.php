<?php 

namespace Core\App;

class Request{

    public $post;
    public $get;
    public $path;
    public $pathData;

    private static $obj;

    public function __construct()
    {
        $this->post = $_POST;
        $this->get = $_GET;
        $this->path = $_SERVER['REQUEST_URI'];
        $this->pathData = explode("/",$this->path );
    }


    public static function get() {
        if(!isset(self::$obj)) {
            self::$obj = new Request();
        }
        return self::$obj;
    }
}