<?php

function redirect($name)
{
	if ($name == 'back') {
		$endpoint = $_SERVER['HTTP_REFERER'];
	} else {
		$endpoint = route($name);
	}
	header("Location: {$endpoint}");
}

function redirectCode($code)
{
	http_response_code($code);
	if(file_exists(__DIR__."../../../views/http/".$code.".php")){
		return view("http.".$code);
	}
}
