<?php

// A simple die and dump function for debugging.
function dd(...$obj)
{
    echo '<pre>';
    foreach($obj as $ob){
		var_dump($ob);
	}
	exit;
    echo '</pre>';
}