<?php 

function session($key, $value = null)
{
	if($value !== null)
	{
		$_SESSION[$key] = $value;
		return $value;
	}
	return key_exists($key,$_SESSION) ? $_SESSION[$key] : "";
}

function csrf()
{
	return session('csrf');
}

function check_csrf()
{
	if(key_exists('csrf', $_POST))
	{
		if($_POST['csrf'] == csrf())
		{
			return true;
		}
	}
	return false;
}

function flash($key, $value = null)
{
	$_SESSION['flash'][$key] = $value;
}

// function old($key){
// 	$oldValue = null;
// 	if(key_exists($key,$_SESSION['old_data'])){
// 		$oldValue = $_SESSION['old_data'][$key];
// 	}
// 	return $oldValue;
// }