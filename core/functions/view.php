<?php

use Core\Utils\Request;

function view(string $viewName, $context=[])
{
	require_once "../core/once/flash.php";
	require_once "../core/once/old.php";
    extract($context);
	$lang = session('lang');
	$changeLang = ($lang == "en" ? "pl" : "en");
	$uri = Request::uri();
	$currentRoute = currentRoute();
    $filePath = str_replace('.', '/', $viewName);
    require "../views/{$filePath}.php";
}