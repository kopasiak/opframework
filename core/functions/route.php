<?php

use Core\Utils\Router;

function route($name)
{
	$router = Router::load('../routes.php');
	$routes = $router->routes();
	return key_exists($name,$routes) ? "/".$routes[$name]['path'] : "";
}

function currentRoute()
{
	$router = Router::load('../routes.php');
	$routes = $router->routes();
	$uri = Core\Utils\Request::uri();
	foreach($routes as $route){
		if($route['path'] == $uri && $route['method'] == 'GET'){
			return  $route['name'];
		}
	}
	return null;
}
