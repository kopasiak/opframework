<?php

class Db extends PDO {

    private static $obj;

    public static function getConn() {
        if(!isset(self::$obj)) {
            self::$obj = new Db('mysql:host='.DB_CONFIG["db_host"].';dbname='.DB_CONFIG["db_name"],DB_CONFIG["db_user"],DB_CONFIG["db_pass"]);
        }
        return self::$obj;
    }
}
