<?php

class Autoloader{

    const TYPE_SEARCH_DEFAULT = 1;
    const TYPE_SEARCH_CORE = 2;
    const TYPE_SEARCH_SOFT = 3;

    const EXCLUDE = [".",".."];

    protected $_files = [];

    public function load(){
        $this->_loadCore();
        $this->_loadSoft();
        require __DIR__."/../config.php";
        foreach($this->_files as $file){
            require $file;
        }
    }

    private function _loadCore(){
        $this->_goDir(__DIR__,self::TYPE_SEARCH_CORE);
    }

    private function _loadSoft(){
        $this->_goDir(__DIR__."/..",self::TYPE_SEARCH_SOFT);
    }
    
    private function _goDir($path, $searchType = self::TYPE_SEARCH_DEFAULT){
        $files = scandir($path);
        foreach($files as $fileName){
            
            if(in_array($fileName,self::EXCLUDE) ||
            ($searchType == self::TYPE_SEARCH_CORE && $fileName == "Autoloader.php") ||
            ($searchType == self::TYPE_SEARCH_CORE && $fileName == "once") ||
            ($searchType == self::TYPE_SEARCH_SOFT && $fileName == "core") ||
            ($searchType == self::TYPE_SEARCH_SOFT && $fileName == "public_html") ||
            ($searchType == self::TYPE_SEARCH_SOFT && $fileName == "views") ||
            ($searchType == self::TYPE_SEARCH_SOFT && $fileName == "routes.php") ||
            ($searchType == self::TYPE_SEARCH_SOFT && $fileName == "run.php") ||
            ($searchType == self::TYPE_SEARCH_SOFT && $fileName == "config.php")){
                continue;
            }
            $filePath = $path."/".$fileName;

            if(is_dir($filePath)){
                $this->_goDir($filePath);
                continue;
            }
            $fileInfo = pathinfo($filePath);
            if(key_exists("extension",$fileInfo) && $fileInfo['extension'] == "php"){
                $this->_files[] = $filePath;
            }
        }
    }
}