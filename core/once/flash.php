<?php

$GLOBALS['flash'] = session("flash");

session("flash",[]);

function getFlash($key)
{
	$flashValue = null;
	if(key_exists($key,$GLOBALS['flash'])){
		$flashValue = $GLOBALS['flash'][$key];
	}
	return $flashValue;
}

