<?php

$GLOBALS['old'] = session("old_data");

session("old_data",[]);

function old($key){
    $oldValue = null;
	if(key_exists($key,$GLOBALS['old'])){
		$oldValue = $GLOBALS['old'][$key];
	}
	return $oldValue;
}