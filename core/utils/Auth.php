<?php

namespace Core\Utils;

class Auth
{

    public static function hash($password)
    {
        return md5($password . "password_salt");
    }

    public static function user()
    {
        return session('auth_user');
    }

    public static function saveAuth($user,$remember = false)
    {
        session('auth_user',$user);
        session('auth_time',time());
        session('auth_remeber',$remember);
    }

    public static function removeAuth()
    {
        session('auth_user',[]);
        session('auth_time',0);
        session('auth_remeber',false);
    }

    public static function checkAuth()
    {
        return !empty(session('auth_user'));
    }

    public static function checkPassword($password, $typedPassword)
    {
        if ($password == self::hash($typedPassword)) {
            return true;
        }
        return false;
    }

    public static function checkAuthSession()
    {
        if (!session('auth_user')) session('auth_user',[]);
        if (!session('auth_time')) session('auth_time',0);
        if (!session('auth_remeber')) session('auth_remeber',false);
        if ((time() - session('auth_time') > 1800 && !session('auth_remeber')) ||
        (time() - session('auth_time') > 36000 && session('auth_remeber'))) {
            self::removeAuth();
        } else {
            $_SESSION['auth_time'] = time();
        }
    }
}
