<?php

namespace Core\Utils;

use Exception;

// A class responsible for mapping requests to its controller.
class Router
{
    protected $middlewares = [
        "global"
    ];

    protected $routes = [
        'GET' => [],
        'POST' => [],
    ];

    public static function load(string $file)
    {
		self::initSession();
        $router = new static();
        require $file;

        return $router;
    }
	
	public static function initSession()
	{
		if(!isset($_SESSION)) 
		{
			session_start();
		}
		if (!session("csrf")) {
            session("csrf",bin2hex(random_bytes(32)));
		}
        if (!session("old_data")) {
            session("old_data",[]);
		}
        if (!session("flash")) {
            session("flash",[]);
		}
	}
	
	public function routes()
	{
		$routes = [];
        foreach($this->routes as $method => $routeGroup){
            foreach($routeGroup as $path => $route){
                $route['method'] = $method;
                $route['path'] = $path;
                $routes[$route['name']] = $route;
            }
        }
        return $routes;
	}

    public function get($uri, $controller, $name,$middlewares = [])
    {
        $this->routes['GET'][$uri] = ['controller' => $controller, 'name' => $name, 'middlewares' => $middlewares];
        return $this;
    }

    public function post($uri, $controller, $name,$middlewares = [])
    {
        $this->routes['POST'][$uri] = ['controller' => $controller, 'name' => $name, 'middlewares' => $middlewares];
        return $this;
    }

    public function direct(string $uri, string $method)
    {
        if (array_key_exists($uri, $this->routes[$method])) {
            $name = $this->routes[$method][$uri]['name'];
            $middlewares = array_merge($this->middlewares,$this->routes[$method][$uri]['middlewares']);
            foreach($middlewares as $middlewareName){
                $middlewareNameFull = "App\\Middleware\\".ucfirst($middlewareName)."Middleware";
                if(!class_exists($middlewareNameFull)){
                    $middlewareNameFull = "Core\\".$middlewareNameFull;
                }
                $middleware = new $middlewareNameFull();
                if(!$middleware->check()){
                    return $middleware->callback();
                }
            }
            
            return $this->callAction(...explode('@', $this->routes[$method][$uri]['controller']));
        }
        return redirectCode("404");
    }

    protected function callAction($controller, $action)
    {
        $controller =  "App\\Controllers\\{$controller}";
        $controller = new $controller;

        if (! method_exists($controller, $action)) {
            throw new Exception("{$controller} does not have {$action}");
        }

        return $controller->$action();
    }
}