<?php

require 'core/Autoloader.php';
(new Autoloader())->load();

$commandName = $argv[1];
$commandNameFull = "App\\Command\\".ucfirst($commandName)."Command";
$command = new $commandNameFull(array_slice($argv,2));
$command->run();
